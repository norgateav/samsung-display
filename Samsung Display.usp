/*******************************************************************************************
  SIMPL+ Module Information
  (Fill in comments below)
*******************************************************************************************/
/*
Dealer Name:
System Name:
System Number:
Programmer:
Comments:

Copyright � 2017 Norgate AV Holdings Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
// #ENABLE_DYNAMIC
#SYMBOL_NAME "Samsung Display"
// #HINT ""
// #DEFINE_CONSTANT
#CATEGORY "46" "Samsung" 
// #DEFAULT_VOLATILE
// #PRINT_TO_TRACE
// #DIGITAL_EXPAND 
// #ANALOG_SERIAL_EXPAND 
// #OUTPUT_SHIFT 
// #HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
/*
#HELP_BEGIN
   (add additional lines of help lines)
#HELP_END
*/

/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
// #CRESTRON_LIBRARY ""
#USER_LIBRARY "NAVFoundation"

/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT	_skip_,osc,command[33]; 
ANALOG_INPUT	volume; 
// STRING_INPUT 
BUFFER_INPUT	from_device[1024]; 

DIGITAL_OUTPUT	_skip_,power_on_fb,power_off_fb,mute_on_fb,mute_off_fb,input_fb[11],aspect_fb[15]; 
ANALOG_OUTPUT	volume_fb; 
STRING_OUTPUT	to_device; 

/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
// TCP_CLIENT
// TCP_SERVER
// UDP_SOCKET

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
INTEGER_PARAMETER	unit_id;
// SIGNED_INTEGER_PARAMETER
// LONG_INTEGER_PARAMETER
// SIGNED_LONG_INTEGER_PARAMETER
// STRING_PARAMETER

/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
/*
#BEGIN_PARAMETER_PROPERTIES parameter_variable, parameter_variable, ...
   // propValidUnits = // unitString or unitDecimal|unitHex|unitPercent|unitCharacter|unitTime|unitTicks;
   // propDefaultUnit = // unitString, unitDecimal, unitHex, unitPercent, unitCharacter, unitTime or unitTicks;
   // propBounds = lower_bound , upper_bound;
   // propDefaultValue = ;  // or, propDefaultValue = "";
   // propList = // { "value" , "label" } , { "value" , "label" } , ... ;
   // propShortDescription = "status_bar_hint_text";
   // #BEGIN_PROP_FULL_DESCRIPTION  line_1...  line_2...  line_n  #END_PROP_FULL_DESCRIPTION
   // #BEGIN_PROP_NOTES line_1...  line_2...  line_n  #END_PROP_NOTES
#END_PARAMETER_PROPERTIES
*/

/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
/*
STRUCTURE MyStruct1
{
};

MyStruct1 struct;
*/

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
volatile INTEGER	iCommandBusy,iSemaphore,iRequiredPower,iActualPower,iRequiredInput,iActualInput,
					iRequiredMute,iActualMute,iLoop,iAutoImageRequired,iVolumeUpdateRequired,
					iRequiredAspect,iActualAspect;
// LONG_INTEGER
volatile SIGNED_INTEGER		siRequiredVolume,siActualVolume;
// SIGNED_LONG_INTEGER
volatile STRING	sHeader[3],sFullStatusResponse[6],sPowerResponse[6],sMuteResponse[6],
				sVolumeResponse[6],sInputResponse[6];

/*******************************************************************************************
  Functions
  (Add any additional functions here)
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/
/*
Function MyFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here
}
*/

/*
Integer_Function MyIntFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here

    Return (0);
}
*/

/*
String_Function MyStrFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here

    Return ("");
}
*/
function Send(string sParam) {
 	to_device = sParam;
}

function Build(string sParam) {
 	string sTemp[50];
 	sTemp = "\xAA" + sParam + chr(NAVCalculateSumOfBytesChecksum(1, sParam));
 	Send(sTemp);
}

function Process() {
	signed_integer siTemp;
	string sTemp[NAV_MAX_BUFFER];
	iSemaphore = 1;
	//trace("Process() Starting");
	while(len(from_device) && NAVContains(from_device,sFullStatusResponse)) {
		sTemp = gather(sFullStatusResponse,from_device,1);
		if(len(sTemp)) {
			//trace("Got full status reponse");
			sTemp = sTemp + gatherbylength(8,from_device);
			while(!NAVStartsWith(sTemp,sFullStatusResponse)) {
				sTemp = NAVStripCharsFromLeft(sTemp,1);
				processlogic();				
			}
			
			switch(byte(sTemp,7)) {
				case(0x00): { iActualPower = 2; }
				case(0x01): { iActualPower = 1; }
			}
	        
	        if(siActualVolume <> byte(sTemp,8)) {
				siActualVolume = byte(sTemp,8);
				iVolumeUpdateRequired = 1;
			}
			 		
			switch(byte(sTemp,9)) {
			 	case(0x00): { iActualMute = 2; }
				case(0x01): { iActualMute = 1; }
			}
			 		
	        switch(byte(sTemp,10)) {
			 	case(0x14): iActualInput = 1;
				case(0x1E): iActualInput = 2;
			 	case(0x18): iActualInput = 3;
			 	case(0x0C): iActualInput = 4;
			 	case(0x04): iActualInput = 5;
			 	case(0x08): iActualInput = 6;
			 	case(0x20): iActualInput = 7;
			 	case(0x1F): iActualInput = 3;
			 	case(0x21): iActualInput = 9;
			 	case(0x22): iActualInput = 9;
			 	case(0x23): iActualInput = 10;
			 	case(0x24): iActualInput = 10;
				case(0x25): iActualInput = 11;
			}
	
			switch(byte(sTemp,11)) {
	            case(0x10): iActualAspect = 1;
	            case(0x18): iActualAspect = 2;
	            case(0x20): iActualAspect = 3;
	            case(0x00): iActualAspect = 4;
	            case(0x01): iActualAspect = 5;
	            case(0x04): iActualAspect = 6;
	            case(0x05): iActualAspect = 7;
	            case(0x06): iActualAspect = 8;
	            case(0x09): iActualAspect = 9;
	            case(0x31): iActualAspect = 10;
	            case(0x0B): iActualAspect = 11;
	            case(0x0C): iActualAspect = 12;
	            case(0x0D): iActualAspect = 13;
	            case(0x0E): iActualAspect = 14;
	            case(0x0F): iActualAspect = 15;
			}
		}
	}
	
	//trace("Process() Exiting");
	iSemaphore = 0;
}

function Feedback() {
	integer x;
 	power_on_fb = (iActualPower = 1);
 	power_off_fb = (iActualPower = 2);

	for(x = 1 to 11) {
 		input_fb[x] = ((iActualPower = 1) && (iActualInput = x));
	}

	for(x = 1 to 15) {
 		aspect_fb[x] = ((iActualPower = 1) && (iActualAspect = x));
	}
 	
 	mute_on_fb = ((iActualPower = 1) && (iActualMute = 1));
 	mute_off_fb = (iActualMute = 2);
 	if(iVolumeUpdateRequired) { volume_fb = siActualVolume * 65535 / 100; iVolumeUpdateRequired = 0; }
}

function Drive() {
	string sTemp[50];
 	iLoop = iLoop + 1;
 	if(iLoop > 4) {
 		iLoop = 1;
 		sTemp = "\x00" + chr(unit_id) + "\x00";
 		Build(sTemp);
 		return;
 	}
 	
 	Feedback();
 	
 	if(iCommandBusy) { return; }
 	
 	if(iRequiredPower && (iRequiredPower = iActualPower)) { iRequiredPower = 0; }
 	if(iRequiredInput && (iRequiredInput = iActualInput)) { iRequiredInput = 0; }
 	if(iRequiredMute && (iRequiredMute = iActualMute)) { iRequiredMute = 0; }
 	if(iRequiredAspect && (iRequiredAspect = iActualAspect)) { iRequiredAspect = 0; }
 	//if((siRequiredVolume >= 0) && (siRequiredVolume = siActualVolume)) { siRequiredVolume = -1; }
 	
 	if(iRequiredMute && (iActualMute <> iRequiredMute) && (iActualPower = 1)) {
 		switch(iRequiredMute) {
 			case(1): { sTemp = "\x13" + chr(unit_id) + "\x01\x01"; }
 			case(2): { sTemp = "\x13" + chr(unit_id) + "\x01\x00"; }
 		}
 		
 		Build(sTemp);
 		iCommandBusy = 1;
 		wait(200) { iCommandBusy = 0; }
 		iLoop = 4;
 		return;
 	}
 	 	
 	if(iRequiredPower && (iActualPower <> iRequiredPower)) {
 		switch(iRequiredPower) {
 			case(1): { sTemp = "\x11" + chr(unit_id) + "\x01\x01"; }
 			case(2): { sTemp = "\x11" + chr(unit_id) + "\x01\x00"; }
 		}
 		
 		Build(sTemp);
 		iCommandBusy = 1;
 		wait(800) { iCommandBusy = 0; }
 		iLoop = 4;
 		return;
 	}
 	
 	if(iRequiredInput && (iActualInput <> iRequiredInput) && (iActualPower = 1)) {
 		switch(iRequiredInput) {
 			case(1): { sTemp = "\x14" + chr(unit_id) + "\x01\x14"; }
 			case(2): { sTemp = "\x14" + chr(unit_id) + "\x01\x1E"; }
 			case(3): { sTemp = "\x14" + chr(unit_id) + "\x01\x18"; }
			case(4): { sTemp = "\x14" + chr(unit_id) + "\x01\x0C"; }
            case(5): { sTemp = "\x14" + chr(unit_id) + "\x01\x04"; }
            case(6): { sTemp = "\x14" + chr(unit_id) + "\x01\x08"; }
            case(7): { sTemp = "\x14" + chr(unit_id) + "\x01\x20"; }
            case(8): { sTemp = "\x14" + chr(unit_id) + "\x01\x1F"; }
            case(9): { sTemp = "\x14" + chr(unit_id) + "\x01\x21"; }
            case(10): { sTemp = "\x14" + chr(unit_id) + "\x01\x23"; }
			case(11): { sTemp = "\x14" + chr(unit_id) + "\x01\x25"; }
 		}
 		
 		Build(sTemp);
 		iCommandBusy = 1;
		//iActualInput = iRequiredInput;
 		wait(600) { iCommandBusy = 0; }
 		iLoop = 4;
 		return;
 	}

	if(iRequiredAspect && (iActualAspect <> iRequiredAspect) && (iActualPower = 1)) {
 		switch(iRequiredAspect) {
 			case(1): { sTemp = "\x15" + chr(unit_id) + "\x01\x10"; }	//RGB 16:9
 			case(2): { sTemp = "\x15" + chr(unit_id) + "\x01\x18"; }	//RGB 4:3
 			case(3): { sTemp = "\x15" + chr(unit_id) + "\x01\x20"; }	//RGB Original Size
			case(4): { sTemp = "\x15" + chr(unit_id) + "\x01\x00"; }	//Auto Wide
            case(5): { sTemp = "\x15" + chr(unit_id) + "\x01\x01"; }	//16:9
            case(6): { sTemp = "\x15" + chr(unit_id) + "\x01\x04"; }	//Zoom
            case(7): { sTemp = "\x15" + chr(unit_id) + "\x01\x05"; }	//Zoom 1
            case(8): { sTemp = "\x15" + chr(unit_id) + "\x01\x06"; }	//Zoom 2
            case(9): { sTemp = "\x15" + chr(unit_id) + "\x01\x09"; }	//Just Scan
            case(10): { sTemp = "\x15" + chr(unit_id) + "\x01\x31"; }	//Wide Zoom
			case(11): { sTemp = "\x15" + chr(unit_id) + "\x01\x0B"; }	//4:3
			case(12): { sTemp = "\x15" + chr(unit_id) + "\x01\x0C"; }	//Wide Fit
			case(13): { sTemp = "\x15" + chr(unit_id) + "\x01\x0D"; }	//Custom
			case(14): { sTemp = "\x15" + chr(unit_id) + "\x01\x0E"; }	//Smart View 1
			case(15): { sTemp = "\x15" + chr(unit_id) + "\x01\x0F"; }	//Smart View 2
 		}
 		
 		Build(sTemp);
 		iCommandBusy = 1;
 		wait(200) { iCommandBusy = 0; }
 		iLoop = 4;
 		return;
 	}
 	
 	/*
 	if((siRequiredVolume >= 0) && (iActualPower = 1)) {
 		sTemp = "\x12" + chr(unit_id) + "\x01" + chr(siRequiredVolume); 		
 		Build(sTemp);
 		siRequiredVolume = -1;
 		iCommandBusy = 1;
 		wait(50) { iCommandBusy = 0; }
 		return;
 	}
 	*/
 	
 	if(iAutoImageRequired && (iActualPower = 1)) {
 		iAutoImageRequired = 0;
 		sTemp = "\x76" + chr(unit_id) + "\x01\x01";
 		Build(sTemp);
 		return;
 	} 	
}

/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
push osc { Drive(); }

push command {
	switch(getlastmodifiedarrayindex()) {
		case(1): { iRequiredPower = 1; Drive(); }
		case(2): { iRequiredMute = 2; iRequiredPower = 2; iRequiredInput = 0; Drive(); }
		case(3): { 
			if(iRequiredPower) { 
				switch(iRequiredPower) {
					case(1): { iRequiredPower = 2; }
					case(2): { irequiredPower = 1; }
				}
			}else {
				switch(iActualPower) {
					case(1): { iRequiredPower = 2; }
					case(2): { iRequiredPower = 1; }
				}
			}
			
			Drive();
		}
		case(4): { iRequiredPower = 1; iRequiredInput = 1; Drive(); }
		case(5): { iRequiredPower = 1; iRequiredInput = 2; Drive(); }
		case(6): { iRequiredPower = 1; iRequiredInput = 3; Drive(); }
		case(7): { iRequiredPower = 1; iRequiredInput = 4; Drive(); }
		case(8): { iRequiredPower = 1; iRequiredInput = 5; Drive(); }
		case(9): { iRequiredPower = 1; iRequiredInput = 6; Drive(); }
		case(10): { iRequiredPower = 1; iRequiredInput = 7; Drive(); }
		case(11): { iRequiredPower = 1; iRequiredInput = 8; Drive(); }
		case(12): { iRequiredPower = 1; iRequiredInput = 9; Drive(); }
		case(13): {
			if((iActualPower = 1) || (iRequiredPower = 1)) {
				iRequiredMute = 1; Drive();
			}
		}
		case(14): {
			if((iActualPower = 1) || (iRequiredPower = 1)) {
				iRequiredMute = 2; Drive();
			}
		}
		case(15): {
			if(iActualPower = 1) {
				if(iRequiredMute) { 
					switch(iRequiredMute) {
						case(1): { iRequiredMute = 2; }
						case(2): { iRequiredMute = 1; }
					}
				}else {
					switch(iActualMute) {
						case(1): { iRequiredMute = 2; }
						case(2): { iRequiredMute = 1; }
					}
				}
				
				Drive();
			}
		}
		case(16): {
			iAutoImageRequired = 1; Drive();
		}
		case(17): { iRequiredPower = 1; iRequiredInput = 10; Drive(); }
		case(18): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 1; Drive();
			}
		}
		case(19): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 2; Drive();
			}
		}
		case(20): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 3; Drive();
			}
		}
		case(21): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 4; Drive();
			}
		}
		case(22): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 5; Drive();
			}
		}
		case(23): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 6; Drive();
			}
		}
		case(24): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 7; Drive();
			}
		}
		case(25): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 8; Drive();
			}
		}
		case(26): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 9; Drive();
			}
		}
		case(27): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 10; Drive();
			}
		}
		case(28): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 11; Drive();
			}
		}
		case(29): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 12; Drive();
			}
		}
		case(30): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 13; Drive();
			}
		}
		case(31): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 14; Drive();
			}
		}
		case(32): { 
			if(iActualPower = 1 || iRequiredPower = 1) {
            	iRequiredAspect = 15; Drive();
			}
		}
		case(33): {
        	iRequiredPower = 1; iRequiredInput = 11; Drive();
		}
	}
}

change volume {
	string sTemp[6];	
	sTemp = "\x12" + chr(unit_id) + "\x01" + chr(volume * 100 / 65535);
	Build(sTemp);
	iLoop = 4;
}

change from_device {
	//trace("Response Received");
	if(!iSemaphore) {
		Process();
	}	
}

/*
PUSH input
{
    // TODO:  Add code here
}
*/

/*
RELEASE input
{
    // TODO:  Add code here
}
*/

/*
CHANGE input
{
    // TODO:  Add code here
}
*/

/*
EVENT
{
    // TODO:  Add code here
}
*/

/*
SOCKETCONNECT
{
    // TODO:  Add code here
}
*/

/*
SOCKETDISCONNECT
{
    // TODO:  Add code here
}
*/

/*
SOCKETRECEIVE
{
    // TODO:  Add code here
}
*/

/*
SOCKETSTATUS
{
    // TODO:  Add code here
}
*/

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main() {
	iSemaphore = 0;
	iCommandBusy = 0;
	siRequiredVolume = -1;
	siActualVolume = -1;
	iAutoImageRequired = 0;
	iRequiredAspect = 0;
	sHeader = "\xAA\xFF" + chr(unit_id);
	sFullStatusResponse = sHeader + "\x09A\x00";
	sPowerResponse = sHeader + "\x03A\x11";
	sMuteResponse = sHeader + "\x03A\x13";
	sVolumeResponse = sHeader + "\x03A\x12";
	sInputResponse = sHeader + "\x03A\x14";
}
